package com.meli.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meli.exception.ControllerException;
import com.meli.exception.ServiceException;
import com.meli.service.MorseService;
import com.meli.util.ConstantsError;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "Morse", description = "Rest Service Morse translate")
public class MorseController {

	private static final Logger LOG = LoggerFactory.getLogger(MorseController.class);
	
	private MorseService morseService;
	
	public MorseController(MorseService morseService) {
		this.morseService = morseService;
	}

	@ApiOperation(value = "Translate from alphabetical string to a morse key sequence")
	@GetMapping("/translate/decodeAlphabetic2Morse")
	public ResponseEntity<?> getTranslateToMorse(@RequestParam(required = false, name = "alphanumeric_code") String chain) throws ServiceException {
		LOG.info("getTranslateToMorse :: Convirtiendo cadena " + chain);
		if (chain == null || chain.equals("")) throw new ControllerException(ConstantsError.ERR_NULL_CHAIR); //Se puede llevar esta validacion a un annotation customizable	
		String result = morseService.getTranslateToMorse(chain);
		LOG.info("getTranslateToMorse :: Cadena: " + chain +  " : " + result);
		return ResponseEntity.ok(result);
	}
	
	@ApiOperation(value = "Translate a Morse Key Sequence into Alphabet String")
	@GetMapping("/translate/decodeMorse2Alphabetic")
	public ResponseEntity<?> getTranslateToAlphabetic(@RequestParam(required = false, name = "morse_code") String chain) throws ServiceException {
		LOG.info("getTranslateToAlphabetic :: Convirtiendo cadena " + chain);
		if (chain == null || chain.equals("")) throw new ServiceException(ConstantsError.ERR_NULL_CHAIR); //Se puede llevar las validaciones a un annotation customizable	
		String result = morseService.getTranslateToAlphabetic(chain);
		LOG.info("getTranslateToAlphabetic :: Cadena: " + chain +  " : " + result);
		return ResponseEntity.ok(result);
	}
	
	@ApiOperation(value = "Translate a binary string into a morse key sequence")
	@GetMapping("/translate/decodeBits2Morse")
	public ResponseEntity<?> getTranslateBinaryToMorse(@RequestParam(required = false, name = "binary_code") String chain) throws ServiceException {
		LOG.info("getTranslateBinaryToMorse :: Convirtiendo cadena " + chain);
		if (chain == null || chain.equals("")) throw new ServiceException(ConstantsError.ERR_NULL_CHAIR); //Se puede llevar las validaciones a un annotation customizable
		String result = morseService.getTranslateBinaryToMorse(chain);
		LOG.info("getTranslateBinaryToMorse :: Cadena: " + chain +  " : " + result);
		return ResponseEntity.ok(result);
	}
	
}
