package com.meli.exception;

public class ServiceException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private String code;
	
	public ServiceException(Throwable t, String code) {
		super(t);
		this.code = code;
	}
	
	public ServiceException(String message, String code) {
		super(message);
		this.code = code;
	}
	
	public ServiceException(String message, String code, Throwable t) {
		super(message, t);
		this.code = code;
	}
	
	public ServiceException(String message) {
		super(message);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
