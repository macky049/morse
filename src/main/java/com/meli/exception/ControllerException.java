package com.meli.exception;

import java.util.ArrayList;

public class ControllerException extends ServiceException {

	private static final long serialVersionUID = 1L;
	
	private ArrayList<String> listError = new ArrayList<>();

	public ControllerException(ServiceException a) {
        super(a.getMessage(), a.getCode(), a);
    }
	
	public ControllerException(String message) {
		super(message);
	}
	
	public ControllerException(String message, String code) {
		super(message, code);
	}
	
	public ControllerException(Throwable t, String code) {
		super(t, code);
	}
	
	public ControllerException(String message, String code, Throwable t) {
		super(message, code, t);
	}

	public ArrayList<String> getListError() {
		return listError;
	}

	public void setListError(ArrayList<String> listError) {
		this.listError = listError;
	}	
}
