package com.meli.repository.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meli.dto.CodeDTO;
import com.meli.repository.MorseRepository;

@Repository
public class MorseRepositoryImpl implements MorseRepository {
	
	private static final Logger LOG = LoggerFactory.getLogger(MorseRepositoryImpl.class);
	
	List<CodeDTO> codes;
	
	public MorseRepositoryImpl() {
		
	}
	
	@PostConstruct
    private void loadCodes(){
		try {
			LOG.info("Inicializando carga de codigos alfabeticos y codigos morse");
			InputStream codeJsons = CodeDTO.class.getResourceAsStream("/config/codes.json");
			CodeDTO[] tokens = new ObjectMapper().readValue(codeJsons, CodeDTO[].class);
			this.codes = Arrays.asList(tokens);
			LOG.info("Finalizacion de carga de codigos alfabeticos y codigos morse");
		} catch (IOException e) {
			this.codes = new ArrayList<>();
		}
	}
	
	public String findCodeMorse(String character) {
		String result = null;
		for (CodeDTO code: codes) {
			if (code.getCodeAlfa().equalsIgnoreCase(character)){
				result = code.getCodeMorse();
				break;
			}
		}
		return result;
	}
	
	public String findCodeAlphabeticByMorse(String codeMorse) {
		String result = null;
		for (CodeDTO code: codes) {
			if (code.getCodeMorse().equalsIgnoreCase(codeMorse)){
				result = code.getCodeAlfa();
				break;
			}
		}
		return result;
	}
	
	public String findCodeAlphabeticByBinary(String codeBinary) {
		String result = null;
		for (CodeDTO code: codes) {
			if (code.getCodeBinary().equalsIgnoreCase(codeBinary)){
				result = code.getCodeMorse();
				break;
			}
		}
		return result;
	}
}
