package com.meli.repository;

public interface MorseRepository {
	
	/**
	 * Retorna el codigo morse correspondiente al caracter alfanumerico
	 * @param character
	 * @return
	 */
	String findCodeMorse(String character);
	
	/**
	 * Retorna el caracter alfanumerico a partir de un codigo morse
	 * @param code
	 * @return
	 */
	String findCodeAlphabeticByMorse(String codeMorse);
	
	/**
	 * Retorna el caracter alfanumerico a partir de una secuencia binaria
	 * @param code
	 * @return
	 */
	String findCodeAlphabeticByBinary(String codeBinary);

}
