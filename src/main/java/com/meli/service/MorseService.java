package com.meli.service;

import com.meli.exception.ServiceException;

public interface MorseService {
	
	/**
	 * Servicio que traduce una cadena alfabetica a una clave morse
	 * 
	 * @param String
	 * @return String
	 */
	String getTranslateToMorse(String chain) throws ServiceException;
	
	
	/**
	 * Servicio que traduce a clave morse una cadena alfabetica
	 * 
	 * @param String
	 * @return String
	 */
	String getTranslateToAlphabetic(String chain) throws ServiceException;
	
	/**
	 * -Servicio que traduce una cadena binaria a clave morse
	 * -Se lee una cadena continua sin espacios en blanco y se define un patron para determinar si es un caracter alfabetico o no.
	 * -Un caracter alfabetico le corresponde 8 digitos. Ejm 01000001 -> "a"
	 * -Los tres primeros digitos (010) indica el inicio de un caracter mayuscula.
	 * -Los tres primeros digitos (011) indica el inicio de un caracter minuscula.
	 * -El algoritmo ignora si el caracter es mayuscula o miniscula, retorna solo minuscula.
	 * 
	 * @param String
	 * @return String
	 */
	String getTranslateBinaryToMorse(String chain) throws ServiceException;

}
