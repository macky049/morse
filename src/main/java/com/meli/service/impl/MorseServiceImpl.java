package com.meli.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.meli.exception.ServiceException;
import com.meli.repository.MorseRepository;
import com.meli.service.MorseService;
import com.meli.util.ConstantsError;

@Service
public class MorseServiceImpl implements MorseService {

	private static final int MAX_SIZE = 8;
	
	private static final Character PULSE = '1';
	
	private static final Character PAUSE = '0';
	
	private static final Character BLANK = ' ';
	
	private MorseRepository morseRepository;	
	
	public MorseServiceImpl(MorseRepository morseRepository) {
		this.morseRepository = morseRepository;
	}
	
	/**
	 * {@inheritDoc} 
	 */
	public String getTranslateToMorse(String chain) throws ServiceException{
		String messaje = chain.toLowerCase();		
		String slash = "/";
		StringBuilder codeMorse = new StringBuilder();	
		for  (int i = 0; i < messaje.length() ; i++) { 
			if (messaje.charAt(i) == BLANK) {
				codeMorse.append(slash);
				codeMorse.append(BLANK);
			} else {
				String result = morseRepository.findCodeMorse(Character.toString(messaje.charAt(i)));
				if (result != null) {
					codeMorse.append(result);
					codeMorse.append(BLANK);
				}
			}
		}		
		return codeMorse.toString().trim();
	}
	
	/**
	 * {@inheritDoc} 
	 */
	public String getTranslateToAlphabetic(String chain) throws ServiceException{
		StringBuilder codeMorse = new StringBuilder();
		String subCodeMorse[] = chain.split(" ");
		int totalMax = subCodeMorse.length;
		 for  (int i = 0; i < totalMax; i++){
			 String result = morseRepository.findCodeAlphabeticByMorse(subCodeMorse[i]);
			 if (result != null) {
				 codeMorse.append(result);
			 }
		 }
		 return codeMorse.toString().trim();
	}
	
	/**
	 * {@inheritDoc} 
	 */
	public String getTranslateBinaryToMorse(String chain) throws ServiceException {
		//inicializamos variables
		List<String> listBits = new ArrayList<>();
		char[] chars = chain.replaceAll(" ", "").toCharArray();
		StringBuilder codeMorse = new StringBuilder();
		//validamos la cadena
		this.validateChain(chain, chars);		
		//Cargamos lista de bits
		for (int i=0; i<chars.length; i++) {
			if (chars[i] == PULSE && i+7 <= chars.length ) {				
					// me quedo con los ultimos 5 digtos de la porcion del numero ejm 01000001 -> 00001
					listBits.add(chain.substring(i+2, i+7)); 
					i = i+6;	
			}				
		}			
		//Recorremos la lista de bits y obtenemos el codigo morse
		for (String secuence: listBits) {
			String result = morseRepository.findCodeAlphabeticByBinary(secuence);
			if (result != null) {
				 codeMorse.append(result);
				 codeMorse.append(BLANK);
			 }
		}
		return codeMorse.toString().trim();
	}
	
	private void validateChain(String chain, char[] chars) throws ServiceException {
		// validamos el tamanio
		if (chain.length() < MAX_SIZE) throw new ServiceException(ConstantsError.ERR_SIZE_MAX_SEQUENCE_BINARY);
		// validamos si los digitos son binarios
		for (int i=0; i<chars.length; i++) {
			if (chars[i] != PULSE & chars[i] != PAUSE) throw new ServiceException(ConstantsError.ERR_INAVLID_SEQUENCE_BINARY);
		}
	}
}
