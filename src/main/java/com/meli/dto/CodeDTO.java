package com.meli.dto;

public class CodeDTO {

	private String codeAlfa;
	
	private String codeMorse;
	
	private String codeBinary;

	public String getCodeAlfa() {
		return codeAlfa;
	}

	public void setCodeAlfa(String codeAlfa) {
		this.codeAlfa = codeAlfa;
	}

	public String getCodeMorse() {
		return codeMorse;
	}

	public void setCodeMorse(String codeMorse) {
		this.codeMorse = codeMorse;
	}

	public String getCodeBinary() {
		return codeBinary;
	}

	public void setCodeBinary(String codeBinary) {
		this.codeBinary = codeBinary;
	}
}
