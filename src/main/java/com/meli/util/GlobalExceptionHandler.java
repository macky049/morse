package com.meli.util;

import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.meli.exception.ControllerException;
import com.meli.exception.ServiceException;

@ControllerAdvice
@Component
public class GlobalExceptionHandler {
	
	private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handle(MethodArgumentNotValidException exception) {
    	LOG.error(exception.getMessage());
    	BindingResult result = exception.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        ApiError dto = new ApiError(ConstantsError.ERR_VALIDATION);    	
        for (FieldError fieldError : fieldErrors) {
            dto.add(fieldError.getObjectName(), fieldError.getField(), fieldError.getDefaultMessage());
        }
        return dto;
    }
    
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handle(ConstraintViolationException exception) {
    	LOG.error(exception.getMessage());    	
    	ApiError dto = new ApiError(ConstantsError.ERR_CONSTRAINT); 
    	for (ConstraintViolation<?> violation : exception.getConstraintViolations()) {
    		dto.add(violation.getRootBeanClass().getName(), violation.getPropertyPath().toString(), violation.getMessage());
    	}
    	return dto;
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handle(ControllerException exception) {
    	LOG.error(exception.getMessage());
    	return new ApiError(exception.getMessage(), exception.getCode());
    }
    
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handle(ServiceException exception) {
    	LOG.error(exception.getMessage());
    	return new ApiError(exception.getMessage(), exception.getCode());
    } 
    
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handle(InvalidFormatException exception) {
    	LOG.error(exception.getMessage());
    	return new ApiError(exception.getMessage(), exception.getOriginalMessage());
    }
    
    
}
