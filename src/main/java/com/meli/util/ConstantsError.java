package com.meli.util;

public class ConstantsError {
	
	public static final String ERR_NULL_CHAIR = "The string cannot be null";
	public static final String ERR_VALIDATION = "Error Validation";	
	public static final String ERR_CONSTRAINT = "Error Constraint";
	public static final String ERR_SIZE_MAX_SEQUENCE_BINARY = "Small chain. Must greater than 8 digits";
	public static final String ERR_INAVLID_SEQUENCE_BINARY = "Invalid binary string";
	
	
}
