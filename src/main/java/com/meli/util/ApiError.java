package com.meli.util;

import java.util.ArrayList;
import java.util.List;

public class ApiError {	
	
	private String code;    
	
	private String message;	
	
	private List<FieldErrorVM> fieldErrors;

	public ApiError(String mensaje) {
		this(mensaje, null);
	}
	
    public ApiError(String message, String code) {
    	this.message = message;
    	this.code = code;
    }
    
    public ApiError(String message, String code, List<FieldErrorVM> fieldErrors) {
        this.message = message;
        this.code = code;
        this.fieldErrors = fieldErrors;
    }

    public void add(String objectName, String field, String message) {
        if (fieldErrors == null) {
        	fieldErrors = new ArrayList<>();
        }
        fieldErrors.add(new FieldErrorVM(objectName, field, message));
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<FieldErrorVM> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldErrorVM> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
}
