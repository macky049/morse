package com.meli.morse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.meli.*")
public class MorseApplication {

	public static void main(String[] args) {
		SpringApplication.run(MorseApplication.class, args);
	}

}
