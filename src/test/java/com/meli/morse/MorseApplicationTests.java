package com.meli.morse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.meli.exception.ServiceException;
import com.meli.service.MorseService;

@SpringBootTest
class MorseApplicationTests {

	private final static String RESULT_ALPHABETICAL = "hola meli";
	private final static String RESULT_MORSE = ".... --- .-.. .-";
	private final static String SEQ_MORSE = ".... --- .-.. .- / -- . .-.. ..";
	private final static String SEQ_BINARY = "01001000000001001111011111110010011000000001000001000000";
	private final static String SEQ_BINARY_ERROR_SIZE = "00111";
	private final static String SEQ_BINARY_INVALID = "013450000011";
	
	@Autowired
	MorseService morseService;
	
	@Test
	void contextLoads() {
	}
	
	@Test
	void translate2HumanTestSuccess() throws ServiceException {
		String result = morseService.getTranslateToAlphabetic(SEQ_MORSE);
		assertEquals(result, RESULT_ALPHABETICAL);
	}
	
	@Test
	void decodeBits2MorseTestSuccess() throws ServiceException {
		String result = morseService.getTranslateBinaryToMorse(SEQ_BINARY);
		assertEquals(result, RESULT_MORSE);
	}
	
	 @Test
	 void decodeBits2MorseTestErrorSize() {
		 Exception exception = assertThrows(
				 ServiceException.class, 
					() -> morseService.getTranslateBinaryToMorse(SEQ_BINARY_ERROR_SIZE));
		 assertTrue(exception.getMessage().contains("Small chain"));
			
	 }
	 
	 @Test
	 void decodeBits2MorseTestInvalidChain() {
		 Exception exception = assertThrows(
				 ServiceException.class, 
					() -> morseService.getTranslateBinaryToMorse(SEQ_BINARY_INVALID));
		 assertTrue(exception.getMessage().contains("Invalid"));
			
	 }
}
