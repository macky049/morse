# API Meli MORSE

### Installation

Prerequisites: 

[Java 1.8 o superior](https://www.java.com/es/download/)

[Gradle](https://gradle.org/)


### Build

``` terminal
1 - Descomprimir morse.zip
2 - Compilar el proyecto Morse mediante el comando ./gradlew clean build, el cual ejecuta los test y genera el archivo morse-service-0.0.1-SNAPSHOT.jar que se encuentra dentro de la carpeta /build/libs
```

### Run

``` terminal
Iniciar API Meli MORSE: java -jar morse-service-0.0.1-SNAPSHOT.jar
```

### Test

API Meli MORSE dispone de la interface Swagger (catalogador de servicios) para poder realizar los test.
La misma se encuentra en la siguiente url **http://localhost:9090/swagger-ui.html**
Para cambiar el puerto de ejecucion modificar la property **server.port** dentro del archivo
**application.properties**

### Endpoints

**http://localhost:9090/translate/decodeAlphabetic2Morse?alphanumeric_code=**

**http://localhost:9090/translate/decodeBits2Morse?binary_code=**

**http://localhost:9090/translate/decodeMorse2Alphabetic?morse_code=**

### DockerFile

Para ejecutar sobre docker el .jar, ejecutar lo siguiente:
1) Crear una carpeta por ejm: "morsedocker" y copiar en ella el Dockerfile y el archivo .jar 
2) ingresar desde consola a la carpeta
2) crear la imagen docker: "docker build -t morsedocker ."
3) crear el contenedor y ejecutar: 
docker run -p 4000:9090 morsedocker (modo interactivo) o
docker run -d -p 4000:9090 morsedocker (modo detachado)
4)una vez iniciado el contenedor ingresar a **http://localhost:4000/swagger-ui.html


## Author

* **Caponera Miguel A.** - *macky049@gmail.com*
