FROM  adoptopenjdk/openjdk13:ubi

# Create app directory
RUN mkdir -p /usr/opt/service

# Copy app
COPY build/libs/morse-service-0.0.1-SNAPSHOT.jar /usr/opt/service/morse-service.jar

EXPOSE 9090

ENTRYPOINT exec java -jar /usr/opt/service/morse-service.jar